package edu.wm.cs.cs301.EricNubbeTinhSon.gui;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;
import edu.wm.cs.cs301.EricNubbeTinhSon.R;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.Constants;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.Explorer;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.MazeHolder;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.MazePanel;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.StatePlaying;

/** Screen for play animation. Toast is used to debug. Energy bar actaully depletes when rotate or move buttons are clicked.
 * @author Tinh Son , Eric Nubbe.
 */

public class PlayAnimationActivity extends AppCompatActivity {

    private Button MT;
    private Button Sol;
    private Button WA;
    private Button Pause;
    private ProgressBar Energy;
    protected MazePanel mazePanel;
    protected StatePlaying sp;
    protected MazeHolder mh;
    static Handler ahandler = new Handler();
    private boolean stopped = false;
    private boolean paused = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.playanimation_activity);

        //sets music on loop
        MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.creepybackground);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();

        //gets maze panel for custom view and the information from singleton Maze Holder and starts State Playing to display on custom view
        mazePanel = findViewById(R.id.mazePanel);
        mh = GeneratingActivity.mazeHolder;
        sp = GeneratingActivity.mazeHolder.getPlayingState();
        if(null != mh.getRobot().getMazeConfiguration()){
            Log.d("mazeConfigPlaying", "mazeConfig in manual activity exists");
            sp.setMazeConfiguration(mh.getRobot().getMazeConfiguration());
            sp.start(mh, mazePanel);
        }


        //Using Toast to debug
        MT = findViewById(R.id.Map);
        Sol = findViewById(R.id.Solution);
        WA = findViewById(R.id.Wall);
        Pause = findViewById(R.id.pause);
        Energy = findViewById(R.id.energyBar);

        Button solveMazeButton = findViewById(R.id.solveMaze);
        solveMazeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paused = false;
                sp.keyDown(Constants.UserInput.ToggleLocalMap, 1);
                sp.keyDown(Constants.UserInput.ToggleFullMap, 1);
                sp.keyDown(Constants.UserInput.ToggleSolution, 1);
                ahandler.removeCallbacks(solveMaze);
                ahandler.postDelayed(solveMaze, 10);
//                solve Solve = new solve();
//                Solve.execute();
            }
        });

        Pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paused = true;
            }
        });

        //ListenerOnClick
        MT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getBaseContext(), "Map Toggle", Toast.LENGTH_LONG).show();
                sp.keyDown(Constants.UserInput.ToggleLocalMap, 1);

            }
        });
        Sol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getBaseContext(), "Solution Toggle", Toast.LENGTH_LONG).show();
                sp.keyDown(Constants.UserInput.ToggleSolution, 1);

            }
        });
        WA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "Wall Toggle", Toast.LENGTH_LONG).show();
                sp.keyDown(Constants.UserInput.ToggleFullMap, 1);

            }
        });
    }


    @Override
    public void onBackPressed(){
        Intent toTitle = new Intent (PlayAnimationActivity.this, AMazeActivity.class);
        toTitle.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(toTitle);
        finish();
    }

    //calls the driver's move to exit method with pauses to update screen
    private Runnable solveMaze = new Runnable() {
        public void run() {
            if(paused == false) {
                if (!stopped) {
                    try {
                        if (mh.getRobot().isAtExit() || mh.getRobot().isOutsideMaze()) {
                            Intent toTitle = new Intent(PlayAnimationActivity.this, WinningActivity.class);
                            toTitle.putExtra("Battery", mh.getRobot().getBatteryLevel());
                            toTitle.putExtra("Odometer", mh.getRobot().getOdometerReading());
                            toTitle.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            stopped = true;
                            startActivity(toTitle);
                            finish();
                        } else if (mh.getRobot().hasStopped()) {
                            Intent toTitle = new Intent(PlayAnimationActivity.this, LosingActivity.class);
                            toTitle.putExtra("Battery", mh.getRobot().getBatteryLevel());
                            toTitle.putExtra("Odometer", mh.getRobot().getOdometerReading());
                            toTitle.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            stopped = true;
                            startActivity(toTitle);
                            finish();

                        } else {
                            sp.keyDown(Constants.UserInput.Drive, 1);
                            mazePanel.postInvalidate();
                        }
                        ahandler.postDelayed(this, 10);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    };

}
