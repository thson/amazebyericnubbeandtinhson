package edu.wm.cs.cs301.EricNubbeTinhSon.generation;

import android.util.Log;

/** Class used to pass information about maze from the generating activity to the playing activities. Uses some methods from the old Controller class
@author Tinh Son and Eric Nubbe
 */
public class MazeHolder{

    private MazeConfiguration mazeConfig;
    private Order.Builder builder;
    private int skill;
    private RobotDriver driver;
    private BasicRobot robot;
    private boolean perfect;
    private StatePlaying sp;
    private StateGenerating sg;


    public MazeHolder(String builder, int skillLevel, String driver, boolean perfect){
        this.builder = convertStringtoBuilder(builder);
        this.skill = skillLevel;
        this.driver = convertStringtoDriver(driver);
        this.perfect = perfect;
        this.sp = new StatePlaying();
    }

    public void setMazeGenerating(StateGenerating g){
        this.sg = g;
    }

    public void switchFromGeneratingToPlaying(MazeConfiguration Config){
        sp.setMazeConfiguration(Config);
        this.mazeConfig = Config;
        this.robot = new BasicRobot();
        this.robot.setMaze(this);
        this.driver.setRobot(this.robot);
    }

    //String driver to RobotDriver
    public RobotDriver convertStringtoDriver(String choice) {
        switch (choice) {
            case "Wizard":
                this.driver = new Wizard(robot);
                break;
            case "Wall-Follower":
                this.driver = new WallFollower(robot);
                break;
            case "Explorer":
                this.driver = new Explorer(robot);
                break;
            case "Manual Driver":
                this.driver = new ManualDriver(robot);
                break;
            case "Pledge":
                this.driver = new Pledge(robot);
                break;
            default:
                this.driver = new ManualDriver(robot);
                break;
        }
        return driver;
    }

    //Convert string builder to Builder
    public Order.Builder convertStringtoBuilder(String choice) {
        switch(choice) {
            case "Prim": return Order.Builder.Prim;
            case "Eller": return Order.Builder.Eller;
            case "DFS": return Order.Builder.DFS;
            default:return Order.Builder.DFS;
        }
    }


    //GETTER METHODS------------------------------------------------------------------->
    public RobotDriver getDriver(){
        return driver;
    }

    public BasicRobot getRobot(){
        return robot;
    }

    public Order.Builder getBuilder(){
        return builder;
    }

    public int getLevel(){
        return skill;
    }

    public boolean getPerfect(){
       return perfect;
    }

    public int getPercentDone(){
        return sg.getPercentDone();
    }

    public MazeConfiguration getMazeConfiguration(){
        Log.d("mazeHolder.getMazeConfiguration", "mazeConfig exists: " + (sp.getMazeConfiguration() != null));
        return sp.getMazeConfiguration();
    }

    public StatePlaying getPlayingState(){
        return sp;
    }

    public StateGenerating getStateGenerating(){
        return sg;
    }

    public int[] getCurrentPosition() {
        return sp.getCurrentPosition();
    }

    public CardinalDirection getCurrentDirection() {
        Log.d("carDir", "No ");
        return sp.getCurrentDirection();
    }

}
