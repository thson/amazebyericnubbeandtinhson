package edu.wm.cs.cs301.EricNubbeTinhSon.gui;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import edu.wm.cs.cs301.EricNubbeTinhSon.R;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.MazePanel;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.Order;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.RobotDriver;

/**Main activity class that takes care of getting information about the maze builder, driver, etc. Consists of spinners and buttons to that transfers activity to either generate new maze or load old maze
 * @author Tinh Son, Eric Nubbe
 */

public class AMazeActivity extends AppCompatActivity {
    private Button generate;
    private Button load;
    private static boolean generated = false;


    //main activity goes here
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(edu.wm.cs.cs301.EricNubbeTinhSon.R.layout.activity_main);

        //sets music on loop
        MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.scarytitle);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();

        //creates spinner for builder
        final Spinner spinner = findViewById(R.id.builderSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.builder, R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        //creates spinner for driver
        final Spinner spinner2 = findViewById(R.id.driverSpinner);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.driver, R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);

        //seekbar for skill level
        final SeekBar sb = findViewById(R.id.skillSeekBar);
        final TextView tv = findViewById(R.id.skillValue);
        sb.setProgress(0);
        sb.setMax(9);
        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
               if(fromUser){
                   if(progress >= 0 && progress <= seekBar.getMax()){
                       String progressString = String.valueOf(progress);
                       tv.setText("Skill Level: " + progressString);
                       seekBar.setSecondaryProgress(progress);
                   }
               }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        //buttonlistener on click goes to generating screen
        generate = findViewById(R.id.newMazeButton);
        generate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                String builder = spinner.getSelectedItem().toString();
                String driver = spinner2.getSelectedItem().toString();
                int skillLevel = sb.getProgress();
                Intent startAct = new Intent(AMazeActivity.this, edu.wm.cs.cs301.EricNubbeTinhSon.gui.GeneratingActivity.class);
                startAct.putExtra( "builder", builder);
                startAct.putExtra( "driver", driver);
                startAct.putExtra("skillLevel", skillLevel);
                startAct.putExtra("generated", false);
                startAct.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(startAct);
                generated = true;
                finish();
            }
        });

        //on click loads saved maze
        load = findViewById(R.id.loadMazeButton);
        load.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(generated){
                    Intent load = new Intent(AMazeActivity.this, GeneratingActivity.class);
                    load.putExtra("generated", true);
                    startActivity(load);
                    finish();
                } else {
                    Toast.makeText(getBaseContext(), "No previous mazefile existed", Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}