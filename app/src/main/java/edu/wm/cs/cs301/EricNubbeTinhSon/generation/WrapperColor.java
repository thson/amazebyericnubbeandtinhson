package edu.wm.cs.cs301.EricNubbeTinhSon.generation;

import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;


public class WrapperColor{
	private Paint painter = new Paint(Paint.ANTI_ALIAS_FLAG);
	//store color values in an array so it can be returned later in appropriate getRGB for android
	private int[] color = new int[3];
	private int sColor;

    public WrapperColor(String colorString, Paint painter){
        this.painter = painter;
        setColor(colorString);
    }
	public WrapperColor(int red, int green, int blue, Paint painter) {
        this.painter = painter;
		setColor(red, green, blue);
	}
	
	public WrapperColor(int bits, Paint painter) {
        this.painter = painter;
		painter.setColor(bits);
		this.sColor = bits;
        Log.d("colorBits", "colorBits is: " + bits);
	}
	public void setColor(){
	    painter.setColor(getRGB());
    }

	public void setColor(String colorString) {
        Log.d("setColorString", colorString);

        switch(colorString.toLowerCase()){
			case "black":
				painter.setColor(Color.BLACK);
				break;
			case "white":
				painter.setColor(Color.WHITE);
				break;
			case "red":
				painter.setColor(Color.RED);
				break;
			case "blue":
				painter.setColor(Color.BLUE);
				break;
			case "yellow":
				painter.setColor(Color.YELLOW);
				break;
			case "gray":
				painter.setColor(Color.GRAY);
				break;
			case "purple":
				painter.setColor(Color.MAGENTA);
				break;
			case "green":
				painter.setColor(Color.GREEN);
				break;
			case "darkgray":
				painter.setColor(Color.DKGRAY);
				break;
		}
	}
		
	public void setColor(int red, int green, int blue) {
		this.color[0] = red;
		this.color[1] = green;
		this.color[2] = blue;
		painter.setColor(getRGB());

	}
	public int getRGB() {
        Log.d("colorRGB", "colorRGB is: " + Color.rgb(color[0], color[1], color[2]) );
		return Color.rgb(color[0], color[1], color[2]);
    }

}
