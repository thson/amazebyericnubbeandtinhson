package edu.wm.cs.cs301.EricNubbeTinhSon.generation;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import edu.wm.cs.cs301.EricNubbeTinhSon.R;

/** A custom view that displays the graphics generated by State Playing. Has static methods so that Seg, FirstPersonDrawer, and
 * others can access methods to change the graphics
 * @author Tinh Son and Eric Nubbe
 */
public class MazePanel extends View{
    private Canvas canvas;
    private static Paint painter = new Paint(Paint.ANTI_ALIAS_FLAG);;
    private Bitmap bitmap;
    private WrapperGraphics wg;
    private static BitmapShader floorShader;
    private static Bitmap floor;
    private static BitmapShader skyShader;
    private static Bitmap sky;
    private static BitmapShader wallShader;
    private static Bitmap wall;

    public MazePanel(Context context){
        super(context);

        //new bitmap to display graphics
        bitmap = Bitmap.createBitmap(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT, Bitmap.Config.ARGB_8888); //each pixel stored in 4 bits
        canvas = new Canvas(bitmap);

        //wall and floor texture through Paint.Shader
        wall = BitmapFactory.decodeResource(getResources(), R.drawable.wall);
        wall = Bitmap.createScaledBitmap(wall, 400, 400, true);
        wallShader =  new BitmapShader(wall, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        floor = BitmapFactory.decodeResource(getResources(), R.drawable.background);
        floor = Bitmap.createScaledBitmap(floor, 400, 400, true);
        floorShader = new BitmapShader(floor, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        sky = BitmapFactory.decodeResource(getResources(), R.drawable.sky);
        sky = Bitmap.createScaledBitmap(sky, 400, 400, true);
        skyShader = new BitmapShader(sky, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
    }
    public MazePanel(Context context, AttributeSet attrs){
        super(context, attrs);

        //new bitmap to display graphics
        bitmap = Bitmap.createBitmap(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT, Bitmap.Config.ARGB_8888); //each pixel stored in 4 bits
        canvas = new Canvas(bitmap);

        //wall and floor texture through Paint.Shader
        wall = BitmapFactory.decodeResource(getResources(), R.drawable.wall);
        wall = Bitmap.createScaledBitmap(wall, 400, 400, true);
        wallShader =  new BitmapShader(wall, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        floor = BitmapFactory.decodeResource(getResources(), R.drawable.background);
        floor = Bitmap.createScaledBitmap(floor, 400, 400, true);
        floorShader = new BitmapShader(floor, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        sky = BitmapFactory.decodeResource(getResources(), R.drawable.sky);
        sky = Bitmap.createScaledBitmap(sky, 400, 400, true);
        skyShader = new BitmapShader(sky, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

    }
    public void update(){
        invalidate();
        Log.v("MazeInvalidate", "MazeInvalidate successfully");
    }

    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        canvas.drawBitmap(bitmap, null, new RectF(0, 0, getWidth(), getHeight()), painter);
        update();
    }

    @Override
    public void onMeasure(int width, int height){
        super.onMeasure(width, height);
        this.setMeasuredDimension(width, height);
    }
//
    public WrapperGraphics getBufferGraphics(){
        if (null == wg){
            this.wg = new WrapperGraphics(canvas, painter, bitmap);
        }
        return wg;
    }

    public static WrapperColor createWrapperColor(int red, int green, int blue) {
        return new WrapperColor(red, green, blue, painter);
    }

    public static WrapperColor createWrapperColor(int bits) {
        return new WrapperColor(bits, painter);
    }

    public static int getWrapperRGB(WrapperColor wc) {
        return wc.getRGB();
    }

    public static void fillGraphicsRect(WrapperGraphics wg, int x, int y, int width, int height) {
        wg.fillRect(x, y, width, height);
    }

    public static void setGraphicsColor(WrapperGraphics wg, String colorName) {
        wg.setColor(colorName);
    }

    public static void setGraphicsColor(WrapperGraphics wg, WrapperColor wc) {
        wg.setColor(wc);
    }

    public static void fillGraphicsPolygon(WrapperGraphics wg, int[] x, int[] y, int n) {
        wg.fillPolygon(x,y,n);
    }

    public static void drawGraphicsLine(WrapperGraphics wg, int x1, int y1, int x2, int y2 ) {
        wg.drawLine(x1, y1, x2, y2 );
    }

    public static void fillGraphicsOval(WrapperGraphics wg, int x, int y, int width, int height) {
        wg.fillOval(x, y, width, height);
    }

    public static void drawBackgroundSky(WrapperGraphics wg, int left, int top, int right, int bottom){
        wg.setBackground(left, top, right, bottom, skyShader);
    }

    public static void drawBackgroundFloor(WrapperGraphics wg, int left, int top, int right, int bottom){
        wg.setBackground(left, top, right, bottom, floorShader);
    }

    public static void setWallShader(WrapperGraphics wg){
        wg.setWallShader(wallShader);
    }

}
