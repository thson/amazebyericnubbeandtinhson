package edu.wm.cs.cs301.EricNubbeTinhSon.generation;
 

/** Searches for the exit of the maze by following the left wall according to its point of view.
 * It is okay at solving mazes but can get stuck
 * @author Tinh Son and Eric Nubbe
 *
 */


public class WallFollower implements RobotDriver{
	private BasicRobot robot;
	
	public WallFollower(Robot r) {
		this.robot = (BasicRobot) r;
	}
	
	@Override
	public void setRobot(Robot r) {
		this.robot = (BasicRobot) r;
	}

	@Override
	public void setDimensions(int width, int height) {
		 
	}

	@Override
	public void setDistance(Distance distance) {
		
	}

	//PseudoCode:
//	Activate left-side distance sensor
//	While not at exit:
//		move forward
//		if DistanceToObstable(left) != 0 
//			turn left
			
	@Override
	public boolean drive2Exit() throws Exception {
		try {
//			while(!robot.isOutsideMaze()) {
			if (robot.distanceToObstacle(Robot.Direction.LEFT) == 0) {
				if (robot.distanceToObstacle(Robot.Direction.FORWARD) != 0){
					robot.move(1, false);
					
				}
				else if (robot.distanceToObstacle(Robot.Direction.FORWARD) == 0){
					if (robot.distanceToObstacle(Robot.Direction.RIGHT) != 0) {
						robot.rotate(Robot.Turn.RIGHT, false);
					} else {
						robot.rotate(Robot.Turn.AROUND, false);
					}
				}
			}
			else if (robot.distanceToObstacle(Robot.Direction.LEFT) != 0) {
				robot.rotate(Robot.Turn.LEFT, false);
				robot.move(1, false);
			} if(robot.hasStopped()) {
				return false;
			}
//		  }
		}
		catch (Exception e) {
		}
		if(robot.isOutsideMaze()) {
			return true;
		}
		return false;
}
		
	@Override
	public float getEnergyConsumption() {
		return robot.getBatteryLevel();
	}

	@Override
	public int getPathLength() {
		return robot.getOdometerReading();
	}

}