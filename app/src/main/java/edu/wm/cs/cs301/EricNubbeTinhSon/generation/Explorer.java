package edu.wm.cs.cs301.EricNubbeTinhSon.generation;

import java.util.Random;

/** This implementation of the Driver automatically searches for the exit of the maze by moving to its neighbor
 * that is has visited least. Ties are broken randomly. Horrible at solving mazes
* @author Tinh Son and Eric Nubbe
* */


public class Explorer implements RobotDriver {
	private BasicRobot robot;
	private int[][] maze_array;
	private int x; 
	private int y;


	public Explorer(Robot r) {
		this.robot = (BasicRobot) r;
	}
	@Override
	public void setRobot(Robot r) {
		this.robot = (BasicRobot) r;
	}

	@Override
	public void setDimensions(int width, int height) {
		this.maze_array = new int[width][height];
	}

	@Override
	public void setDistance(Distance distance) {
	}
	
	/*Psuedocode
	Instantiate a 2D array with size of maze
	Marks its current position in the array
	While not at exit(
		if exit visible:
		 	move to exit
		if not in room:
			Use DistanceToObstacle() to see which direction it can move forward
			Move to neighboring square least visited (randomly choose in case of tie)
			mark the position with number of times it has visited the position as it moves. 
		if in room:
			Look for all door cells
			Move to door that has been visited least 
		check if at exit*/
	@Override
	public boolean drive2Exit() throws Exception {
		setDimensions(robot.getController().getMazeConfiguration().getWidth(), robot.getController().getMazeConfiguration().getHeight());
		while (!robot.isAtExit()) {
			if(robot.hasStopped()) {
				return false;
			}
			this.x = robot.getCurrentPosition()[0];
			this.y = robot.getCurrentPosition()[1];

			int[] move_dir = getSquareValues();
			moveToSquare(move_dir);
		}
		if(robot.hasStopped()) {
			return false;
		}
		return true;
	  } 

	@Override
	public float getEnergyConsumption() {
		return robot.getBatteryLevel();
	}

	@Override
	public int getPathLength() {
		return robot.getOdometerReading();
	}
	
	public void moveToSquare(int[] move_dir) {
		int least_visited = 0;
		while(move_dir[least_visited] < 0) { //make least visited first number that is not -1
			least_visited ++;
			}
		int min = move_dir[least_visited];
		for (int i = least_visited + 1; i < move_dir.length; i++) {
			if (move_dir[i] < min && move_dir[i] >= 0) {
				min = move_dir[i];
				least_visited = i;
				}
			else if (move_dir[i] == min) {
			    Random random = new Random();
			    boolean rand = random.nextBoolean();
			    if (rand) { least_visited = i;}
					}
				}

		switch (least_visited) {
		case 0://North
			switch(robot.getFront()) {
			case BACKWARD: walk();
							break;
			case FORWARD: robot.rotate(Robot.Turn.AROUND, false);
						  robot.move(1,false);
						  break;
			case LEFT: robot.rotate(Robot.Turn.LEFT, false);
					   walk();
					   break;
			case RIGHT:robot.rotate(Robot.Turn.RIGHT, false);
			   		   walk();
			   		   break;
			default: break;
			}
			break;

		case 1: //South
			switch(robot.getFront()) {
			case BACKWARD: robot.rotate(Robot.Turn.AROUND, false);
						   walk();
						   break;
			case FORWARD: robot.move(1,false);
						  break;
			case LEFT: robot.rotate(Robot.Turn.RIGHT, false);
			   		   walk();
			   		   break;
			case RIGHT:robot.rotate(Robot.Turn.LEFT, false);
				   	   walk();
				   	   break;
			default: break;
			}
			break;

		case 2: 	//West
			switch(robot.getFront()) {
			case BACKWARD: robot.rotate(Robot.Turn.RIGHT, false);
						   walk();
						   break;
			case FORWARD: robot.rotate(Robot.Turn.LEFT, false);
						  robot.move(1,false);
						  break;
			case LEFT: walk();
			   		   break;
			case RIGHT:robot.rotate(Robot.Turn.AROUND, false);
				   	   walk();
				   	   break;
			default: break;
			}
			break;

		case 3: 		//East
			switch(robot.getFront()) {
			case BACKWARD: robot.rotate(Robot.Turn.LEFT, false);
						   walk();
						   break;
			case FORWARD: robot.rotate(Robot.Turn.RIGHT, false);
						  robot.move(1,false);
						  break;
			case LEFT: robot.rotate(Robot.Turn.AROUND, false);
					   walk();
			   		   break;
			case RIGHT:walk();
				   	   break;
			default: break;
			}
			break;

		default: break;
		}

		maze_array[robot.getCurrentPosition()[0]][robot.getCurrentPosition()[1]]++;

			}
	
	public int[] getSquareValues() {

		int[] move_dir = new int[4]; 
		for (int i = 0; i < 4; i++) {
			move_dir[i] = -1;
			}

		if (!robot.getController().getMazeConfiguration().hasWall(x,y,CardinalDirection.North)) {
			if (y-1 >= 0) {
				move_dir[0] = maze_array[x][y-1];
				}
			}
		if (!robot.getController().getMazeConfiguration().hasWall(x,y,CardinalDirection.South)) {
			if (y+1 <= robot.getMazeConfiguration().getHeight()) {
				move_dir[1] = maze_array[x][y+1];
				}
			}
		if (!robot.getController().getMazeConfiguration().hasWall(x,y,CardinalDirection.West)){
			if (x-1 >=0) {
				move_dir[2] = maze_array[x-1][y];
				}
			}
		if (!robot.getController().getMazeConfiguration().hasWall(x,y,CardinalDirection.East)) {
			if (x+1 <= robot.getMazeConfiguration().getWidth()) {
				move_dir[3] = maze_array[x+1][y];
				}
			} return move_dir;
		
	}

	public void walk() {
		robot.move(1, false);;
	}

}