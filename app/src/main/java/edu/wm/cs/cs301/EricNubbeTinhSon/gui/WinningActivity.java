package edu.wm.cs.cs301.EricNubbeTinhSon.gui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import edu.wm.cs.cs301.EricNubbeTinhSon.R;

/**Displays the winning screen with the statistics of the game and an option to go back to title
 * @author Tinh Son , Eric Nubbe.
 */

public class WinningActivity extends AppCompatActivity {
    private Button main;
    TextView energy;
    TextView odometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(edu.wm.cs.cs301.EricNubbeTinhSon.R.layout.winning_activity);

        this.energy = findViewById(R.id.Energy);
        this.odometer = findViewById(R.id.Travelled);

        float energy_level  = getIntent().getFloatExtra("Battery", 0);
        int odometer = getIntent().getIntExtra("Odometer",0);

        this.energy.setText("Battery: " + String.valueOf(Math.round(energy_level)));
        this.odometer.setText("Distance : " + String.valueOf(odometer));

        main = findViewById(R.id.TitleScreen);
        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startAct = new Intent(WinningActivity.this, edu.wm.cs.cs301.EricNubbeTinhSon.gui.AMazeActivity.class);
                startActivity(startAct);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed(){
        Intent toTitle = new Intent (WinningActivity.this, AMazeActivity.class);
        toTitle.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(toTitle);
        finish();
    }
}
