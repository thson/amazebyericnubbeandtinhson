package edu.wm.cs.cs301.EricNubbeTinhSon.gui;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import edu.wm.cs.cs301.EricNubbeTinhSon.R;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.Constants;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.MazeHolder;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.MazePanel;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.Robot;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.StatePlaying;

/** Screen for play animation. Toast is used to debug. Energy bar actaully depletes when rotate or move buttons are clicked.
 * @author Tinh Son , Eric Nubbe.
 */

public class PlayManuallyActivity extends AppCompatActivity {
    private Button MT;
    private Button Sol;
    private Button WA;
    private Button ZI;
    private Button ZO;
    private ImageButton F;
    private ImageButton L;
    private ImageButton RI;
    private ProgressBar Energy;
    protected MazePanel mazePanel;
    private StatePlaying sp;
    private MazeHolder mh;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.playmanually_activity);

        //sets music on loop
        MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.creepybackground);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();

        //gets maze panel for custom view and the information from singleton Maze Holder and starts State Playing to display on custom view
        mazePanel = findViewById(R.id.mazePanel);
        mh = GeneratingActivity.mazeHolder;
        sp = GeneratingActivity.mazeHolder.getPlayingState();
        if(null != mh.getRobot().getMazeConfiguration()){
            Log.d("mazeConfigPlaying", "mazeConfig in manual activity exists");
            sp.setMazeConfiguration(mh.getRobot().getMazeConfiguration());
            sp.start(mh, mazePanel);
        }


        //Using Toast to debug
        MT = findViewById(R.id.Map);
        Sol = findViewById(R.id.Solution);
        WA = findViewById(R.id.Wall);
        ZI = findViewById(R.id.ZoomIn);
        ZO = findViewById(R.id.ZoomOut);
        F = findViewById(R.id.forward);
        L = findViewById(R.id.left);
        RI = findViewById(R.id.right);
        Energy = findViewById(R.id.energyBar);

        //ListenerOnClick
        MT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getBaseContext(), "Map Toggle", Toast.LENGTH_LONG).show();
                sp.keyDown(Constants.UserInput.ToggleLocalMap, 1);
            }
        });
        Sol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getBaseContext(), "Solution Toggle", Toast.LENGTH_LONG).show();
                sp.keyDown(Constants.UserInput.ToggleSolution, 1);
            }
        });
        WA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getBaseContext(), "Wall Toggle", Toast.LENGTH_LONG).show();
                sp.keyDown(Constants.UserInput.ToggleFullMap, 1);
            }
        });
        ZI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getBaseContext(), "Zoom In", Toast.LENGTH_LONG).show();
                sp.keyDown(Constants.UserInput.ZoomIn, 1);
            }
        });
        ZO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getBaseContext(), "Zoom Out", Toast.LENGTH_LONG).show();
                sp.keyDown(Constants.UserInput.ZoomOut, 1);
            }
        });
        F.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getBaseContext(), "Forward", Toast.LENGTH_LONG).show();
                Energy.setProgress(Energy.getProgress() - 5);
                sp.keyDown(Constants.UserInput.Up, 1);
                mh.getRobot().move(1, true);
                if (mh.getRobot().hasStopped()){
                    Intent toTitle = new Intent (PlayManuallyActivity.this, LosingActivity.class);
                    toTitle.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(toTitle);
                    finish();
                }
                mh.getRobot().getCurrentPosition();
                if (mh.getRobot().isOutsideMaze()){
                    Intent toTitle = new Intent (PlayManuallyActivity.this, WinningActivity.class);
                    toTitle.putExtra("Battery", mh.getRobot().getBatteryLevel());
                    toTitle.putExtra("Odometer", mh.getRobot().getOdometerReading());
                    toTitle.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(toTitle);
                    finish();

                }
            }
        });
        L.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getBaseContext(), "Turn Left", Toast.LENGTH_LONG).show();
                Energy.setProgress(Energy.getProgress() - 3);
                sp.keyDown(Constants.UserInput.Left, 1);
                mh.getRobot().rotate(Robot.Turn.LEFT, true);
                if (mh.getRobot().hasStopped()){
                    Intent toTitle = new Intent (PlayManuallyActivity.this, LosingActivity.class);
                    toTitle.putExtra("Battery", mh.getRobot().getBatteryLevel());
                    toTitle.putExtra("Odometer", mh.getRobot().getOdometerReading());
                    toTitle.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(toTitle);
                    finish();
                }
            }
        });
        RI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getBaseContext(), "Turn Right", Toast.LENGTH_LONG).show();
                Energy.setProgress(Energy.getProgress() - 3);
                sp.keyDown(Constants.UserInput.Right, 1);
                mh.getRobot().rotate(Robot.Turn.RIGHT, true);
                if (mh.getRobot().hasStopped()){
                    Intent toTitle = new Intent (PlayManuallyActivity.this, LosingActivity.class);
                    toTitle.putExtra("Battery", mh.getRobot().getBatteryLevel());
                    toTitle.putExtra("Odometer", mh.getRobot().getOdometerReading());
                    toTitle.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(toTitle);
                    finish();
                }
            }
        });

        mazePanel.update();
    }

    @Override
    public void onBackPressed(){
        Intent toTitle = new Intent (PlayManuallyActivity.this, AMazeActivity.class);
        toTitle.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(toTitle);
        finish();
    }
}
