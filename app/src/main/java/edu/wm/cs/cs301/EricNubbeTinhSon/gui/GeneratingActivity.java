package edu.wm.cs.cs301.EricNubbeTinhSon.gui;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import edu.wm.cs.cs301.EricNubbeTinhSon.R;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.ManualDriver;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.MazeConfiguration;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.MazeFactory;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.MazeHolder;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.MazePanel;
import edu.wm.cs.cs301.EricNubbeTinhSon.generation.StateGenerating;

/**contains place holder loading screen that still takes in extra intents like builder, skill level, and driver to later be passed into background generation later. Logcat is used to see if the info is passed through
 * @author Tinh Son , Eric Nubbe.
 */
public class GeneratingActivity extends AppCompatActivity {

    static MazeConfiguration mazeConfig;
    static String builder;
    static String driver;
    int skillLevel;
    static boolean generated;
    int status = 0;
    private Handler handler = new Handler();
    TextView v;
    MazePanel panel; //maybe
    public static MazeHolder mazeHolder;
    public StateGenerating sg;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(edu.wm.cs.cs301.EricNubbeTinhSon.R.layout.generating_activity);

        //pulls out saved maze if it exists
        generated = getIntent().getBooleanExtra("generated", true);
        panel = new MazePanel(getApplicationContext());
        Log.v("generated", "Previous Maze has been generated: " + generated);
        if(generated == false){
            this.builder = getIntent().getStringExtra("builder");
            this.driver = getIntent().getStringExtra("driver");
            this.skillLevel = getIntent().getIntExtra("skillLevel", 0);
            mazeHolder = new MazeHolder(builder, skillLevel, driver, false);
            //builder, driver and skill level tag
            Log.d("builder", builder);
            Log.d("driver", driver);
            Log.d ("skillLevel", "Level is :" + skillLevel);
            progressUpdate();

        } else { //takes values from title spinners to build the maze with
            Log.v("mazeHolderLoad", "Builder is " + mazeHolder.getBuilder());
            Log.v("mazeHolderLoad", "SkillLevel is " + mazeHolder.getLevel());
            Log.v("mazeHolderLoad", "Driver is " + mazeHolder.getDriver());
            if (driver.equals("Manual")) {
                Intent toPlay = new Intent(GeneratingActivity.this, PlayManuallyActivity.class);
                toPlay.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(toPlay);
                finish();
            }
            else {
                Intent toPlay = new Intent(GeneratingActivity.this, PlayAnimationActivity.class);
                toPlay.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(toPlay);
                finish();
            }
        }

    }
    //draws and updates spinning progress wheel
    public void progressUpdate() {
        Resources res = getResources();
        Drawable drawable = res.getDrawable(R.drawable.progressbar);
        final ProgressBar progress = findViewById(R.id.circularProgressbar);
        progress.setProgress(0);   // Main Progress
        progress.setSecondaryProgress(100); // Secondary Progress
        progress.setMax(100); // Maximum Progress
        progress.setProgressDrawable(drawable);
        v = findViewById(R.id.tv);
        final MazeHolder mh = mazeHolder;
        final MazePanel p = panel;

        new Thread(new Runnable() {
            @Override
            public void run() {
                sg = new StateGenerating(mazeHolder.getBuilder(), mazeHolder.getLevel(), mazeHolder.getPerfect());
                sg.start(mh, p);
                mh.setMazeGenerating(sg);
                while (status < 100) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    status  = mh.getPercentDone();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                        progress.setProgress(status);
                        v.setText(status + "%");
                        }
                    });
                }
                    if (driver.equals("Manual")) {
                        Intent toPlay = new Intent(GeneratingActivity.this, PlayManuallyActivity.class);
                        toPlay.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(toPlay);
                        finish();
                    }
                    else {
                        Intent toPlay = new Intent(GeneratingActivity.this, PlayAnimationActivity.class);
                        toPlay.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(toPlay);
                        finish();
                    }
                }
        }).start();
    }

    @Override
    public void onBackPressed(){
        Intent toTitle = new Intent (GeneratingActivity.this, AMazeActivity.class);
        toTitle.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(toTitle);
        finish();
    }
}
