package edu.wm.cs.cs301.EricNubbeTinhSon.generation;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;



public class WrapperGraphics{
	private Canvas canvas;
	private Bitmap bitmap;
	private Paint painter;
    private BitmapShader wallShader;
	public WrapperGraphics(Canvas canvas, Paint painter, Bitmap bitmap){
        this.painter = painter;
        this.bitmap = bitmap;
        this.canvas = canvas;
    }

    public void setColor(String colorString) {
        switch(colorString.toLowerCase()){
            case "black":
                painter.setColor(Color.BLACK);
                break;
            case "white":
                painter.setColor(Color.WHITE);
                break;
            case "red":
                painter.setColor(Color.RED);
                break;
            case "blue":
                painter.setColor(Color.BLUE);
                break;
            case "yellow":
                painter.setColor(Color.YELLOW);
                break;
            case "gray":
                painter.setColor(Color.GRAY);
                break;
            case "purple":
                painter.setColor(Color.MAGENTA);
                break;
            case "green":
                painter.setColor(Color.GREEN);
                break;
            case "darkgray":
                painter.setColor(Color.DKGRAY);
                break;
            case "floor":
//                Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.background);
//                fillBMPshader = new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
//                mPaint.setShader(fillBMPshader);
                break;
        }
    }

	public void setColor(WrapperColor wc) {
		wc.setColor();
        Log.d("methodsinwg", "in setColor");
    }

	public void fillRect(int x, int y, int w, int h) {
		Rect rect = new Rect(x, y, x + w, y + h);
		canvas.drawRect(rect, painter);
        Log.d("methodsinwg", "in fillRect");

    }

	public void fillPolygon(int[] x, int[] y, int n) {
		painter.setShader(wallShader);
        painter.setStyle(Paint.Style.FILL);
        Path path = new Path();
        path.moveTo(x[0], y[0]);
        for (int i = 1; i < n; i++) {
            path.lineTo(x[i], y[i]);
        }
        path.lineTo(x[0], y[0]);
        canvas.drawPath(path, painter);
        painter.setShader(null);
        Log.d("methodsinwg", "in fillPolygon");
	}

	public void drawLine(float x1, float y1, float x2, float y2) {
        canvas.drawLine(x1, y1 , x2, y2, painter);
        Log.d("methodsinwg", "in drawLine");

    }

	public void fillOval(int x, int y, int w, int h) {
		painter.setStyle(Paint.Style.FILL);
		RectF oval = new RectF(x, y, x + w, y + h);
		canvas.drawOval(oval, painter);
        Log.d("methodsinwg", "in fillOval");

    }

    public void setBackground(int l, int t, int r, int b, BitmapShader shader){
        painter.setShader(shader);
        fillRect(l, t, r, b);
        painter.setShader(null);
    }

    public void setWallShader(BitmapShader wallShader){
        this.wallShader = wallShader;
    }

}
