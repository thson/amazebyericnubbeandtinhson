package edu.wm.cs.cs301.EricNubbeTinhSon.gui;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import edu.wm.cs.cs301.EricNubbeTinhSon.R;

/**Displays the losing screen with the statistics of the game and an option to go back to title
 * @author Tinh Son , Eric Nubbe.
 */

public class LosingActivity extends AppCompatActivity {
    private Button main;
    TextView energy;
    TextView odometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(edu.wm.cs.cs301.EricNubbeTinhSon.R.layout.losing_activity);

        //plays sound
        MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.scream);
        mediaPlayer.start();

        this.energy = findViewById(R.id.Energy2);
        this.odometer = findViewById(R.id.Travelled2);

        float energy_level  = getIntent().getFloatExtra("Battery", 0);
        int odometer = getIntent().getIntExtra("Odometer",0);

        this.energy.setText("Battery: " + String.valueOf(Math.round(energy_level)));
        this.odometer.setText("Distance : " + String.valueOf(odometer));

        main = findViewById(R.id.TitleScreen);
        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startAct = new Intent(LosingActivity.this, edu.wm.cs.cs301.EricNubbeTinhSon.gui.AMazeActivity.class);
                startActivity(startAct);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed(){
        Intent toTitle = new Intent (LosingActivity.this, AMazeActivity.class);
        toTitle.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(toTitle);
        finish();
    }
}
